from beamlinetools.callbacks.file_exporter import *

import os
import h5py


from bluesky.callbacks import CallbackBase

class McaCallback(CallbackBase):
    """Callback for the MCA detector.

    This callback save the mca data in the 
    correct Experiment/set folder.
    It runs only if in the detectors list there is 'mca'
    When the stop document is created the data from mca1 is saved
    We should decide how to include the other mca detectors,
    and how to deal with scans that move more than one motor 
    (at the moment the callback would simply fail)

    """
    def __init__(self):

        self._mca_is_present = False
        self._cached_start = None

    def start(self, doc):

        self._cached_start = doc
        try:
            self.motor_name = list(doc['motors'])
        except KeyError as e:
            # we set motor name to None, 
            # in case the plan was count and no motors are present
            self.motor_name=["None"]
            pass

        try:
            self.detectors  = list(doc['detectors'])
        except KeyError as e:
            print("detector not found")
            pass
        
        if 'mca' in self.detectors:
            self._mca_is_present = True
            self.point_number = 0
    
    def event(self, doc):
        """ We have to do what we do in stop here. Fine to save only the spectrum in channel 1
        
        We need scanNumber_pointNumber.
        
        """
        if self._mca_is_present:
            # retrieve exported_data_dir folder
            exported_data_dir = self._cached_start['exported_data_dir']
            # retrieve base name
            base_name = self._cached_start['base_name']
            #create mca folder if does not exist
            mca_folder = os.path.join(exported_data_dir, 'mca')
            if not os.path.exists(mca_folder):
                print(f"Save folder {mca_folder}")
                os.makedirs(mca_folder)
            # retrieve scan ID and pad it with zeros (6 digits in total)
            scan_id = "{:06d}".format(self._cached_start['scan_id'])
            point_n = "{:06d}".format(self.point_number)
            # construct new mcs filename
            mca_filename = os.path.join(mca_folder, f"{base_name}_{scan_id}_{point_n}.h5")
                  
            scan = doc['data']
            # fetch the data of first channel
            
            if 'mca_spectrum1' in scan:
                mca = scan['mca_spectrum1']
            
                # account for the case no motor was moved
                if self.motor_name[0] == "None":
                    motor_pos = 0
                else:
                    motor_pos = scan[self.motor_name[0]]
                
                with h5py.File(mca_filename, 'w') as hf:
                    scan = hf.create_group('1D Scan')
                    dset = hf.create_dataset("1D Scan/XRF data",  data=mca)
                    dset.attrs.create('DATASET_TYPE', "Raw XRF counts")
                    
                    dset = hf.create_dataset("1D Scan/X Positions",  data=motor_pos)
                    dset.attrs.create('DATASET_TYPE', "X")
                    dset.attrs.create('Motor info', self.motor_name[0])
                #increment point number for the next point
                self.point_number += 1


    def stop(self, doc):
        self._mca_is_present = False
        self._cached_start = None
