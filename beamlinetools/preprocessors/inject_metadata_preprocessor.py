from bluesky.preprocessors import inject_md_wrapper, msg_mutator, ChainMap
import yaml



class FetchExperimentMetadata:
    """
    A configurable preprocessor for adding metadata to a run from a file every time we do a run.
    This allows the file to be updated with new metadata without having to restart the entire experiment.

    Example usage:
    from beamlinetools.preprocessors.inject_metadata_preprocessor import FetchExperimentMetadata
    RE.preprocessors.append(FetchExperimentMetadata(filepath="/opt/bluesky/beamlinetools/beamlinetools/beamline_config/experiment_config.yaml"))


    """
    def __init__(self, filepath=None):
        if filepath is None:
           self.filepath = "/opt/bluesky/beamlinetools/beamlinetools/beamline_config/experiment_config.yaml"
        else:
            self.filepath = filepath

    def __call__(self, plan):
        """
        Insert messages into a plan.

        Parameters
        ----------
        plan : iterable or iterator
            a generator, list, or similar containing `Msg` objects
        """
        
        # read the dictionary from the file
        # Add the metadata from the experiment config file
        
        with open(self.filepath) as stream:
            experiment_config = yaml.safe_load(stream)

        plan = inject_md_wrapper(plan,experiment_config)

        return (yield from plan)