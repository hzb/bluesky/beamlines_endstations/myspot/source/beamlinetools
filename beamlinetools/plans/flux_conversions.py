a0 = 1.04851089179264e+18
a1 = -1.92374299063625e+17
a2 = 1.7174085867336e+16

b1 = -1.52179772805207e+15
b2 = 111099601071380.0
b3 = -3018636603372.41
b0 = 9.36132866242755e+15
b4 = 33604435758.285
b5 = 0

def convert_to_abs_flux_ionization(energy, kth01, scale_factor=5):
    """Convert current from ionization chamber to absolute flux
    
    Parameters:
    energy (float): Energy in keV
    kth01 (float): Current in kth01

    Return:
    f1 (float): Absolute flux in photons per second
    """

    x    = energy
    flux = kth01*(a0+a1*x+a2*x**2) * scale_factor
    
    return flux

def convert_to_abs_flux_diode(energy, kth00):
    """Convert current from diode to absolute flux
    
    Parameters:
    energy (float): Energy in keV
    kth00 (float): Current of kth00

    Return:
    flux (float): Absolute flux in photons per second
    """
    x    = energy
    flux = -1* kth00*(b0+b1*x+b2*x**2+b3*x**3+b4*x**4+b5*x**5)

    return flux