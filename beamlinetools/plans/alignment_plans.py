from bluesky.plans import relative_scan as dscan
import bluesky.plan_stubs as bps


# from beamline
from beamlinetools.beamline_config.base import bec
from beamlinetools.beamline_config.beamline import dcm, kth01

from bluesky_queueserver import parameter_annotation_decorator
@parameter_annotation_decorator({
        "parameters": {
            "cr2ropi_range_u_deg":{
                "default": 1,
                "min": 0,
                "max": 1000,
                "step": 1,
            },
            "steps":{
                "default":20,
                "min": 0,
                "max": 50,
                "step": 1,
            },
            "offset":{
                "default":0,
                "min": -1,
                "max": 1,
                "step": 0.001,
            }
        }
    })
def align_second_crystal_pitch(cr2ropi_range_u_deg:float=1, steps:int=20, offset:float=0, md=None):
    """
    This plan will scan the dcm 2nd crystal ropi position and then move to the center of the peak.
    
    Parameters
    ------------
    cr2ropi_range : float, default 1
        degrees either side of the current position to scan
    steps : int, default 20
        number of steps to take
    offset : float, default 0
        offset to add to the maximum peak position to move to
    md : dict, optional

    -------
    example usage:
        RE(align_second_crystal(steps=50, offset=0.5)  # scan 50 steps, sets the final position to half of the fwhm

    """
    _md = {'plan_name': 'align_second_crystal',
           'detectors': [kth01.name],
           'scan_type': 'alignment',
           'hints': {}
        }
    _md.update(md or {})    
    yield from dscan([kth01], dcm.cr2ropi, -cr2ropi_range_u_deg/1000, cr2ropi_range_u_deg/1000, steps, md=_md)
    peaks_dict = bec.peaks
    max_pos = peaks_dict['max']['kth01'][0]
    if peaks_dict['fwhm']['kth01']:
        fwhm = peaks_dict['fwhm']['kth01'][0]
        max_pos += fwhm*offset

   
    yield from bps.mv(dcm.cr2ropi, max_pos)
   

from bluesky_queueserver import parameter_annotation_decorator
@parameter_annotation_decorator({
        "parameters": {
            "cr2roll_range_u_deg":{
                "default": 100,
                "min": 0,
                "max": 1000,
                "step": 1,
            },
            "steps":{
                "default":20,
                "min": 0,
                "max": 50,
                "step": 1,
            },
            "offset":{
                "default":0,
                "min": -1,
                "max": 1,
                "step": 0.001,
            }
        }
    })

def align_second_crystal_roll(cr2roll_range_u_deg:float=100, steps:int=20, offset:float=0, md=None):
    """
    This plan will scan the dcm 2nd crystal ropi position and then move to the center of the peak.
    
    Parameters
    ------------
    cr2roll_range_u_deg : float, default 1
        degrees either side of the current position to scan
    steps : int, default 20
        number of steps to take
    offset : float, default 0
        offset to add to the maximum peak position to move to
    md : dict, optional

    -------
    example usage:
        RE(align_second_crystal(steps=50, offset=0.5)  # scan 50 steps, sets the final position to half of the fwhm

    """
    _md = {'plan_name': 'align_second_crystal',
           'detectors': [kth01.name],
           'scan_type': 'alignment',
           'hints': {}
        }
    _md.update(md or {})    
    yield from dscan([kth01], dcm.cr2roll, -cr2roll_range_u_deg/1000, cr2roll_range_u_deg/1000, steps, md=_md)
    peaks_dict = bec.peaks
    max_pos = peaks_dict['max']['kth01'][0]
    if peaks_dict['fwhm']['kth01']:
        fwhm = peaks_dict['fwhm']['kth01'][0]
        max_pos += fwhm*offset

   
    yield from bps.mv(dcm.cr2roll, max_pos)
   