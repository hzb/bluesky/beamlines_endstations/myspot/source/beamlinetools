from ..beamline_config.beamline import *
from bluesky.utils import (
    separate_devices,
    all_safe_rewind,
    Msg,
    ensure_generator,
    short_uid as _short_uid,
)
import bluesky.plan_stubs as bps
from bluesky.plans import rel_scan as _rel_scan
from bluesky import preprocessors as bpp
import time
from ophyd.status import Status

from bluesky.plans import count as _count
from bluesky.plans import grid_scan as _grid_scan
from bluesky.plans import scan as _scan
from larch.xray import *

from bluesky.preprocessors import run_wrapper
from bluesky.plan_stubs import kickoff,sleep, wait, abs_set, stop

import yaml
import numpy as np

from beamlinetools.plans.dcm_optimization_parameters import get_cr2ropi_c2roll
from .qserver_plans import edge_list, element_list

def gen_exafs_energies_int_times(edge:str, element:str, pre_edge_step_size_eV:float = 5, pre_edge_integration_time:float=2,edge_step_size_eV:float = 1, edge_integration_time:float=5,exafs_start_r_A:float=3, exafs_stop_r_A:float=12,exafs_num_points:int=200, exafs_start_integration_time:float=5,exafs_end_integration_time:float=10 ):
    """
    This function generates the energies in KeV and the integration times of the detectors
    to perform an exafs measurement around a given edge
    
    Three regions are specified

    - pre_edge  : a coarse linearly spaced region between e0 - 50ev : e0 - 20eV
    - edge      : a finer linearly spaced region between e0 - 20eV : exafs_start 
    - exafs     : an exponentially spaced region between exafs_start :exafs_end
    
    Each region joins the next.
    """
    
    # Determine the energy e0 from the element and edge
    edge_object = xray_edge(element, edge)
    e0 = edge_object.energy/1000 # KeV because MySpot
    
    
    #Define the pre_edge start and stop energie
    pre_edge_start_energy =  e0 - 0.05 #KeV
    pre_edge_stop_energy = e0 - 0.02 #KeV
    
    pre_edge_num = np.ceil((np.abs(pre_edge_stop_energy-pre_edge_start_energy))/(pre_edge_step_size_eV/1000))+1 # do we need +1?

    pre_edge_energies = np.linspace(pre_edge_start_energy, pre_edge_stop_energy, int(pre_edge_num))
    pre_edge_integration_times = (pre_edge_energies*0)+ pre_edge_integration_time


    pre_edge_energies = np.linspace(pre_edge_start_energy, pre_edge_stop_energy, int(pre_edge_num))
    pre_edge_integration_times = (pre_edge_energies*0)+ pre_edge_integration_time

    # exafs calcs
    k = np.linspace(exafs_start_r_A, exafs_stop_r_A,exafs_num_points)
    a = (exafs_end_integration_time - exafs_start_integration_time)/((k[-1]**2)-(k[0]**2))
    c = exafs_start_integration_time - a*(k[0]**2)
    
    exafs_energies = e0+((k**2)/262.4682917)
    exafs_integration_times = a*(k**2)+c 

    # edge calcs
    edge_start_energy = pre_edge_energies[-1]
    edge_stop_energy = exafs_energies[0]
    edge_num = np.floor((np.abs(edge_stop_energy-edge_start_energy))/(edge_step_size_eV/1000))
    edge_energies = np.linspace(edge_start_energy,edge_stop_energy, int(edge_num))
    edge_integration_times = (edge_energies*0)+ edge_integration_time

    total_energies = np.hstack(( pre_edge_energies,edge_energies,exafs_energies )).ravel()
    total_integration_times = np.hstack(( pre_edge_integration_times,edge_integration_times,exafs_integration_times)).ravel()
    
    return total_energies, total_integration_times

try:
    from beamlinetools.beamline_config.beamline import mca, kth00, kth01, gas_system, reactor_cell, dcm

    xas_detectors = [mca, kth00, kth01]
    se_detectors = [gas_system.massflow_contr1, gas_system.massflow_contr2, gas_system.massflow_contr3, gas_system.backpressure_contr1, reactor_cell.temperature_sam, reactor_cell.temperature_reg ]

    from bluesky_queueserver import parameter_annotation_decorator
    @parameter_annotation_decorator({
        "parameters": {
            "element": {
                "annotation": "elements",
                "enums": {"elements": element_list},
                "convert_device_names": False,
            },
            "edge": {
                "annotation": "edges",
                "enums": {"edges": edge_list},
                "convert_device_names": False,
            },
            "start_eV": {
                "default": -50,
                "min": -1000,
                "max": 1000,
                "step": 0.1,
            },
            "stop_eV": {
                "default": -20,
                "min": -1000,
                "max": 1000,
                "step": 0.1,
            },
            "step_size_eV": {
                "default": 5,
                "min": 0,
                "max": 100,
                "step": 0.1,
            },
        }
    })
    def xas_single_region(element, edge,start_eV:float=-50,stop_eV:float=-20, step_size_eV:float = 5, integration_time:float=2, md:dict=None):

        """
        This plan will perform an XAS measurement in a single region

        Arguments:

            element: str 
                The element to scan at

            edge: str 
                The edge to use

            start_eV: float 
                The number of eV below the edge to start at

            stop_eV: float
                The number of eV below the edge to stop at
            
            step_size_eV: float
                The size in eV of each step of the scan
            
            integration_time: float
                The integration time in seconds of the mca detector
        """

        edge_object = xray_edge(element, edge)
        start_energy_KeV = (edge_object.energy + start_eV)/1000 #KeV
        stop_energy_KeV = (edge_object.energy+ stop_eV)/1000 #KeV

        num = np.ceil((np.abs(stop_energy_KeV-start_energy_KeV))/(step_size_eV/1000))+1

        _md = {'plan_name': 'xas_single_region',
            'detectors': [det.name for det in se_detectors + xas_detectors],
            'plan_args': {
                'element' : element,
                'edge': edge,
                'start_eV': start_eV,
                'stop_eV': stop_eV,
                'step_size_eV':step_size_eV,
                'integration_time': integration_time,
            },
            'techniques':['NXxas'],
            'num_points': num,
            'hints': {}
            }
        _md.update(md or {})

        # set detector integration time
        yield from abs_set(mca.preset_real_time, integration_time, wait=True)

        print(f"start_energy = {start_energy_KeV}, stop_energy = {stop_energy_KeV}, step_size = {step_size_eV}, num= {num}")
        
        # check if optimized positions for ropi and roll are available        
        cr2ropi_position, cr2roll_position = get_cr2ropi_c2roll(element, edge)
        if cr2ropi_position is None or cr2roll_position is None:
            print(f"Optimized positions for c2ropi {cr2ropi_position} and/or c2roll{cr2roll_position} not found, using current positions.")
        else:
            yield from bps.mv(dcm.cr2ropi,cr2ropi_position, dcm.cr2roll, cr2roll_position )

        yield from _scan(xas_detectors + se_detectors, dcm.p.energy, start_energy_KeV, stop_energy_KeV,num,md=_md)  

    from bluesky_queueserver import parameter_annotation_decorator
    @parameter_annotation_decorator({
        "parameters": {
            "element": {
                "annotation": "elements",
                "enums": {"elements": element_list},
                "convert_device_names": False,
            },
            "edge": {
                "annotation": "edges",
                "enums": {"edges": edge_list},
                "convert_device_names": False,
            },
            "pre_edge_step_size_eV": {
                "default": 5,
                "min": 0,
                "max": 100,
                "step": 0.1,
            },
            "pre_edge_integration_time": {
                "default": 2,
                "min": 0,
                "max": 100,
                "step": 0.1,
            },
            "edge_step_size_eV": {
                "default": 1,
                "min": 0,
                "max": 100,
                "step": 0.1,
            },
            "edge_integration_time": {
                 "default": 5,
                "min": 0,
                "max": 100,
                "step": 0.1,
            },
            "exafs_start_r_A":{
                 "default": 3,
                "min": 0,
                "max": 10,
                "step": 0.1,
            },
            "exafs_stop_r_A":{
                 "default": 12,
                "min": 2,
                "max": 14,
                "step": 0.1,
            },
            "exafs_num_points":{
                "default": 200,
                "min": 10,
                "max": 1000,
                "step": 0.1,
            },
            "exafs_start_integration_time":{
                 "default": 1,
                "min": 1,
                "max": 10,
                "step": 0.1,
            },
             "exafs_end_integration_time":{
                "default": 10,
                "min": 2,
                "max": 30,
                "step": 0.1,
            },

        }
    })
    def exafs(edge:str, element:str, pre_edge_step_size_eV:float = 5, pre_edge_integration_time:float=2,edge_step_size_eV:float = 1, edge_integration_time:float=5,exafs_start_r_A:float=3, exafs_stop_r_A:float=12,exafs_num_points:int=200, exafs_start_integration_time:float=5,exafs_end_integration_time:float=10, md=None ):
    
        

        energies, int_times = gen_exafs_energies_int_times(edge,element, pre_edge_step_size_eV, pre_edge_integration_time,edge_step_size_eV, edge_integration_time,exafs_start_r_A, exafs_stop_r_A,exafs_num_points, exafs_start_integration_time,exafs_end_integration_time)

        _md = {'plan_name': 'exafs',
            'detectors': [det.name for det in se_detectors + xas_detectors],
            'plan_args': {
                'element' : element,
                'edge': edge,
            },
            'techniques':['NXxas'],
            'num_points': len(energies),
            'hints': {}
            }
        _md.update(md or {})

        def measurement_step(energy:float, int_time:float):
            
            # configure the integration time of the mca            
            yield from bps.abs_set(mca.preset_real_time, int_time, wait=True)
                
            #move the monochromator to the requested energy
            yield from bps.mov(dcm.p.energy, energy)

            # add a checkpoint so that we can pause nicely
            yield from bps.checkpoint()
            
            #trigger and read from all the detectors
            yield from bps.one_shot(se_detectors+xas_detectors+[dcm.p.energy])
        
        @bpp.run_decorator(md=_md)
        def inner_plan():
            
            #Zip the energies and int_times so that we can itterate over the pairs
            points = zip(energies, int_times)

            #For each energy int_time pair, set the int_time and then read the devices
            for point in points:
                yield from measurement_step(point[0], point[1])
                yield from bps.checkpoint()

        # check if optimized positions for ropi and roll are available        
        cr2ropi_position, cr2roll_position = get_cr2ropi_c2roll(element, edge)
        if cr2ropi_position is None or cr2roll_position is None:
            print(f"Optimized positions for c2ropi {cr2ropi_position} and/or c2roll{cr2roll_position} not found, using current positions.")
        else:
            yield from bps.mv(dcm.cr2ropi,cr2ropi_position, dcm.cr2roll, cr2roll_position )

        return (yield from inner_plan())


except Exception as e: print(e)



try:
    from beamlinetools.beamline_config.beamline import filter3
    
    filter_list = ["None", "Al 2mm(was 4, one felt down)", "Al 2mm", "Al 1mm", "Al 0.5mm", "Al 0.2mm", "Al 0.06mm", "Au 0.008", "Cu 0.4","Cu 0.3", "Cu 0.2mm", "Cu 0.1mm", "Cu 0.065mm", "Cu 0.034mm", "Mo 0.01", "Pt 0.0075mm", "Fe2O3", "NiO", "CuO"]
    filter_dict = {"None":1, "Cu 0.2mm":2, "Cu 0.1mm":3, "Cu 0.065mm":4, "Al 2mm(was 4, one felt down)":5, "Al 2mm":6, "Al 1mm":7, "Al 0.5mm":8, "Al 0.2mm":9, "Al 0.06mm":10, "Cu 0.034mm":11, "Pt 0.0075mm":12, "Au 0.008":13, "Mo 0.01":14, "Cu 0.3":15, "Cu 0.4":16, "Fe2O3": 20, "NiO":22, "CuO":24}
    from bluesky_queueserver import parameter_annotation_decorator
    @parameter_annotation_decorator({
        "parameters": {
            "absorber": {
                "annotation": "absorbers",
                "enums": {"absorbers": filter_list},
                "convert_device_names": False,
            },
        }
    })
    def select_absorber(absorber, md:dict=None):

        """
        This plan will move the selected absorber into the beam.

        Arguments:

            filter: str 
                The filter to select
        """

        _md = {'plan_name': 'select_absorber',
            # 'detectors': [det.name for det in se_detectors + xas_detectors],
            'plan_args': {
                'absorber' : absorber,
            },
            # 'techniques':['NXxas'],
            # 'num_points': num,
            'hints': {}
            }
        _md.update(md or {})


        # set detector integration time
        yield from bps.mv(filter3, filter_dict[absorber], wait=True)

        print(f"Absorber {absorber} in position")


except Exception as e: print(e)
