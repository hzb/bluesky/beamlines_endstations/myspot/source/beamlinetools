dcm_optimization_xas_dict = {
    "Ce": {"L3":{"energy":5.723,
                 "c2ropi": 0.1019,
                 "c2roll": 0.002,
                 }
            },
    "Fe": {"K":{"energy":7.112,
                 "c2ropi": 0.1025,
                 "c2roll": 0.001,
                }
            },
    "Ni": {"K":{"energy":8.333,
                 "c2ropi": 0.1028,
                 "c2roll": -0.002,
                }
            },
    "Cu": {"K":{"energy":8.979,
                 "c2ropi": 0.103,
                 "c2roll": -0.008,
                }
            },
    "Sr": {"K":{"energy":16.105,
                 "c2ropi": 0.1044,
                 "c2roll": -0.023,
                }
            },
    "Mo": {"K":{"energy":20,
                 "c2ropi": 0.1054,
                 "c2roll": -0.02,
                }
            },
    "diffraction": {"energy":18,
                 "c2ropi": 0.1045,
                 "c2roll": -0.023,
                },
}

def get_cr2ropi_c2roll(material, edge=None):
    """
    Return the c2ropi and c2roll for the given material and edge.
    """
    if edge is not None:
        if material in dcm_optimization_xas_dict and edge in dcm_optimization_xas_dict[material]:
            return dcm_optimization_xas_dict[material][edge]["c2ropi"], dcm_optimization_xas_dict[material][edge]["c2roll"]
        else:
            return None, None
    else:
        if material in dcm_optimization_xas_dict:
            return dcm_optimization_xas_dict["diffraction"]["c2ropi"], dcm_optimization_xas_dict["diffraction"]["c2roll"]
        else:
            return None, None