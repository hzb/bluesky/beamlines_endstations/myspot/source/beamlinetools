from beamlinetools.beamline_config.beamline import accelerator
from beamlinetools.beamline_config.beamline import adam, diode_z, beamstop
from beamlinetools.beamline_config.beamline import kth00, kth01, dcm

import bluesky.plan_stubs as bps

from beamlinetools.plans.flux_conversions import convert_to_abs_flux_ionization, convert_to_abs_flux_diode

def close_rollo():
    """
    Close the rollo shutter in front of Eiger.
    """
    yield from bps.mv(adam.ch1, 1, adam.ch0, 1)
    yield from bps.sleep(3)
    yield from bps.mv(adam.ch1, 0, adam.ch0, 0)
    yield from bps.sleep(1)

def open_rollo():
    """
    Open the rollo shutter in front of Eiger.
    """
    yield from bps.mv(adam.ch1, 1, adam.ch2, 1)
    yield from bps.sleep(3)
    yield from bps.mv(adam.ch1, 0, adam.ch2, 0)
    yield from bps.sleep(1)


from bluesky_queueserver import parameter_annotation_decorator
@parameter_annotation_decorator({
        
    })
def check_beamstop(md=None):

    """
    This plan will check if the beamstop is in the correct position.
    
    -------
    example usage:
        RE(check_beamstop())

    """
    # close_very_fast_shutter
    yield from bps.mv(adam.ch5,0)

    # close the rollo in front of Eiger
    yield from close_rollo()
    
    # move the diode in
    yield from bps.mv(diode_z, diode_z.in_position)

    # move the beamstop out
    yield from bps.mv(beamstop.y, beamstop.out_position)
    
    # open the very fast shutter
    yield from bps.mv(adam.ch5,1)
    
    yield from bps.sleep(1)

    # Read from detectors and calculate absolute flux
    diode_current      = yield from bps.rd(kth00.readback)
    ionization_current = yield from bps.rd(kth01.readback)

    energy = yield from bps.rd(dcm.p.energy.readback)
    
    diode_flux1      = convert_to_abs_flux_diode(energy, diode_current)
    ionization_flux1 = convert_to_abs_flux_ionization(energy, ionization_current)

    # move the beamstop in
    yield from bps.mv(beamstop.y, beamstop.in_position)
    
    yield from bps.sleep(1)

    diode_current      = yield from bps.rd(kth00.readback)
    ionization_current = yield from bps.rd(kth01.readback)
    
    diode_flux2      = convert_to_abs_flux_diode(energy, diode_current)

    # close_very_fast_shutter
    yield from bps.mv(adam.ch5,0)

    if True:#ionization_flux1 > 10E5 and diode_flux1 > 10E5 and diode_flux2 < 10E8:
        
        # move the diode out
        yield from bps.mv(diode_z, diode_z.out_position)

        # open the rollo in front of Eiger
        yield from open_rollo()

        # open_very_fast_shutter
        yield from bps.mv(adam.ch5,1)
        
        
    else: 
        # close_very_fast_shutter
        yield from bps.mv(adam.ch5,0)

        # move the diode in
        yield from bps.mv(diode_z, diode_z.in_position)

        raise Exception(f"Beamstop check failed: please check diode and beam position. Very Fast Shutter is closed: diode_flux1 {diode_flux1} and diode_flux2 {diode_flux2} and ionization_flux1 {ionization_flux1}")

from bluesky_queueserver import parameter_annotation_decorator
@parameter_annotation_decorator({
        
    })
def switch_to_xas(md = None):

    # move the diode in
    yield from bps.mv(diode_z, diode_z.in_position)

    # close the rollo in front of Eiger
    yield from close_rollo()

    # beamstop out
    yield from bps.mv(beamstop.y, beamstop.out_position)

from bluesky_queueserver import parameter_annotation_decorator
@parameter_annotation_decorator({
        
    })
def switch_to_xrd(md=None):

    yield from check_beamstop()

    #if the test is passed then:
    # beamstop in
    yield from bps.mv(beamstop.y, beamstop.in_position)

    # open the rollo in front of Eiger
    yield from open_rollo()

    # move the diode out
    yield from bps.mv(diode_z, diode_z.out_position)

    

    


