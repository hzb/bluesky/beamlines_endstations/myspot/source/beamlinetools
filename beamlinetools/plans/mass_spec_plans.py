from bluesky.utils import (
    separate_devices,
    all_safe_rewind,
    Msg,
    ensure_generator,
    short_uid as _short_uid,
)
import bluesky.plan_stubs as bps
from bluesky.plans import rel_scan as _rel_scan
from bluesky.plans import count as _count
from bluesky import preprocessors as bpp
from ophyd.status import Status

from bluesky.plan_stubs import abs_set
from bluesky_queueserver import parameter_annotation_decorator
import numpy as np
import time
from ophyd_async.core import SignalRW 
from ophyd.sim import motor
from beamlinetools.plans.dcm_optimization_parameters import get_cr2ropi_c2roll

from beamlinetools.beamline_config.base import bec

from typing import List, Tuple, Literal


#Define Detectors
try:
    from beamlinetools.beamline_config.beamline import gas_analysis

    import warnings


    
    def _configure_mass_spec(**kwargs):
        """
        This internal plan is used to configure the mass spec for a given mode
        
        Parameters
        ------------
        kwargs: arguments
            Depending on the device, diffent parameters can be configured

        -------

        """

        config_group = 'mass_spec_config'
        mass_spec = gas_analysis.mass_spec
        for sig,value in kwargs.items():
            if not hasattr(mass_spec,sig):
                warnings.warn(f"Mass spec has no signal called {sig}")
                continue
            
            sig = getattr(mass_spec,sig)
            
            if not isinstance(sig,SignalRW):  
                warnings.warn(f"Signal/Attribute {sig} is not of type  SignalRW")
                continue
            
            yield from bps.abs_set(sig,value,group=config_group,wait=False)
            
            
        yield from bps.wait(group=config_group)


    def _prime_mass_spec(conf):
        """
        This internal plan is used to set up the mass spec for either bar or mid scan

        It ensures that the device is ready to be triggered and reports the correct shape data
        
        Parameters
        ------------
        conf: dict
            the configuration dictionary to be written to the device

        -------

        """

        # stoping the device
        yield from bps.stop(gas_analysis.mass_spec)

        # write the configuration
        yield from _configure_mass_spec(**conf)
            
      
        
        # trigger it with scan_cycle set to continuous: 
        yield from bps.trigger(gas_analysis.mass_spec,wait=False)


        # since we dont wait for the trigger, but the mass_spec takes some time to configure 
        # we need to wait until it is ready to handle new messages, i.e. until it is prepared
        yield from gas_analysis.mass_spec.wait_for_prepared()

    @parameter_annotation_decorator({
        "parameters": {
            "measurement_device": {
                "annotation": "devices",
                "enums": {"devices": ["FARADAY","SEM"]},
                "convert_device_names": False,
            },
            "start_mass": {
                "default": 1,
                "min": 1,
                "max": 400
            },
            "end_mass":{
                "default": 20,
                "min": 1,
                "max": 400
            },
            "mass_increment":{
                "default": 1,
                "min": 1,
                "max": 400
            }

        }
    })    
    def configure_mass_spec_bar(measurement_device,start_mass:int=1, end_mass:int=20, mass_increment:int=1):
        """
        This plan configure the mass spec in bar mode. After this is run, you can use the detector in a plan 
        
        Parameters
        ------------
        measurement_device : enum
            'FARADAY' or 'SEM". The measurement device used in the scan
        start_mass : int
            the start mass to measure from in the bar scan (default = 1)
        end_mass : int
            the end mass of the bar scan (default is 20)
        mass_increment : int 
            the increment in the bar scan (default is 1)

        -------

        """
        barscan_conf = {
            'mode':'BAR_SCAN', # Enum element <0>
            'start_mass':start_mass,
            'end_mass':end_mass,
            'mass_increment':mass_increment,
            'signal_measurement_device':measurement_device,
            'scan_cycle':'CONTINUOUS'
        }

        yield from _prime_mass_spec(barscan_conf)

    # TO-DO: This plan uses strings and eval because we couldn't work out a nice way of defining a list of tuples of (measurment_device:bool,mass:float)
    # in the gui. For now, you pass strings. 
    
    def configure_mass_spec_mid(measurement_devices:str,masses:str):
        """
        This plan configure the mass spec in bar mode. After this is run, you can use the detector in a plan 
        
        Parameters
        ------------
        measurement_devices : string
            a list represented as a string for example "[0,1,0]". Defines the measurement device to be
            used at each mass in the mid scan

            0 - > FARADAY
            1 - > SEM
        masses : string
            a list represented as a string for the masses to be used in the mid scan

        -------
        example

        # Measure masses 2,4,10 and 12 in an MID scan using measurement device SEM for mass 4 and FARADAY for all others
        RE(configure_mass_spec_mid("[0,1,0,0]","[2,4,10,12]"))

        """
        print(masses)
        print(measurement_devices)
        mid_descriptor = {
        'mass':eval(masses),
        'device':eval(measurement_devices)
        }
            

        midscan_conf = {
            'mode':'MID_SCAN', # Enum element <1>
            'mid_descriptor':mid_descriptor,
            'scan_cycle':'CONTINUOUS'
            
        }


        yield from _prime_mass_spec(midscan_conf)



    def test_mas_spec(num:int = 5):
        for iteration in range(0,num):
            print("-------------------------------------------------------")
            print(f'Iteration: {iteration}')

            print("Configuring for BAR_SCAN")
            yield from configure_mass_spec_bar("FARADAY",1,20,1)

            
            yield from _count([gas_analysis],5,5)

            print("Configuring for MID_SCAN")
            yield from configure_mass_spec_mid(measurement_devices=[0,0,0,1],masses=[1,2,10,15])

            yield from _count([gas_analysis],5,5)

        yield from bps.stop(gas_analysis.mass_spec)
except Exception as e: print(e)