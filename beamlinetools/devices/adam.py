from ophyd import Device, EpicsSignal
from ophyd import Component as Cpt

class Adam(Device):
    """This device has no statuses. We have no idea when a plan is executed.
    So we use ad hoc settle times"""
    ch0 = Cpt(EpicsSignal, "SDO0", name='ch0')
    ch1 = Cpt(EpicsSignal, "SDO1", name='ch1')
    ch2 = Cpt(EpicsSignal, "SDO2", name='ch2')
    ch3 = Cpt(EpicsSignal, "SDO3", name='ch3')
    ch4 = Cpt(EpicsSignal, "SDO4", name='ch4')
    ch5 = Cpt(EpicsSignal, "SDO5", name='ch5')

