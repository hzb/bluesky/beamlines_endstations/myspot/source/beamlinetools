
from ophyd import Device, EpicsMotor, EpicsSignalRO
from ophyd import Component as Cpt

from ophyd import PseudoPositioner, PseudoSingle
from ophyd.pseudopos import pseudo_position_argument,  real_position_argument

import numpy as np

mono_table = {'Si111':
                {'monoh':25, 
                 'monod':3.1356, 
                 'c1height':0.0, 
                 'monoz':-38.0, 
                 'encoder_zero':-0.5328, 
                 'cr2ropi':0.104, 
                 'cr1roll': 0.078,
                 'cr2roll':0.075,
                 'cr2vetrCorr':0.0038},
            'Si311':
                {'monoh':25, 
                 'monod':1.63751, 
                 'c1height':5.718, 
                 'monoz':0.0, 
                 'encoder_zero':-0.399, 
                 'cr2ropi':0.025, 
                 'cr1roll': 0.0278,
                 'cr2roll':0.0776, 
                 'cr2vetrCorr':0.0},
            'B4CML':
                {'monoh':5,
                 'monod':20.0,
                 'c1height':6.46,
                 'monoz':41.5, 
                 'encoder_zero':-0.377, 
                 'cr2ropi':0.027, 
                 'cr1roll': 0.053,
                 'cr2roll':0.205, 
                 'cr2vetrCorr':0.0020}
            }

class DCMEnergyHeight(PseudoPositioner):
    ''' This class implements apertures with four blades (top, bottom, left, right)
    and four pseudo motors (htop,hoffset,vgap,voffset)
    '''

    # constants
    hc_over_e = 12.398419111
    
    # otherspseudo_pos.height
    mono_type = 'Si111'
    mode      = "all_motors"
    
    # The pseudo positioner axes:
    energy    = Cpt(PseudoSingle, egu='KeV')
    height    = Cpt(PseudoSingle, egu = 'mm')
    dummy     = Cpt(PseudoSingle)

    # The real (or physical) positioners:

    cr2latr        = Cpt(EpicsMotor, 'l0201003')
    monobr         = Cpt(EpicsMotor, 'l0202000')    
    cr2vetr        = Cpt(EpicsMotor, 'l0202001')

    


    def set_mono_type(self, verbose=False):
        if hasattr(self, "parent"):
            if hasattr(self.parent, "monoz"):    
                monoz = self.parent.monoz.user_readback.get()
                if monoz < -15:
                    mono_type = "Si111"
                elif monoz > 15:
                    mono_type = "B4CML"
                else:
                    mono_type = "Si311"
                if verbose:
                    print(f"mono_type {mono_type}")

                if mono_type in mono_table.keys():
                    self.mono_type=mono_type
                else:
                    raise ValueError('This mono type does not exist')
            else: 
                print("has no monoz")   
                self.mono_type = "Si111"
    
    def _setup_move(self, *args):
        self.set_mono_type()
        super()._setup_move(*args)
        
    def _calc_real_motor_pos(self, pseudo_pos):
        if self.mode == "all_motors":
            bragg_angle    = np.rad2deg(np.arcsin(self.hc_over_e/(2*mono_table[self.mono_type]["monod"]*pseudo_pos.energy)))
            cr2latr_value  = -1*pseudo_pos.height/(2*np.sin(np.deg2rad(bragg_angle)))
            cr2vetr_value  = pseudo_pos.height/(2*np.cos(np.deg2rad(bragg_angle))) + mono_table[self.mono_type]['c1height']+ mono_table[self.mono_type]['cr2vetrCorr']*cr2latr_value
        elif self.mode == "exafs_mode":
            bragg_angle    = np.rad2deg(np.arcsin(self.hc_over_e/(2*mono_table[self.mono_type]["monod"]*pseudo_pos.energy)))
            cr2latr_value  = self.cr2latr.user_readback.get() # not changed
            cr2vetr_value  = pseudo_pos.height/(2*np.cos(np.deg2rad(bragg_angle))) + mono_table[self.mono_type]['c1height']+ mono_table[self.mono_type]['cr2vetrCorr']*cr2latr_value
        elif self.mode == "channelcut":
            bragg_angle    = np.rad2deg(np.arcsin(self.hc_over_e/(2*mono_table[self.mono_type]["monod"]*pseudo_pos.energy)))
            cr2latr_value  = self.cr2latr.user_readback.get() # not changed 
            cr2vetr_value  = self.cr2vetr.user_readback.get() # not changed
        else:
            raise ValueError(f"The mode {self.mode} is not allowd. Use only all_motors, exafs_mode or channelcut")

        return bragg_angle, cr2latr_value, cr2vetr_value
    
    @pseudo_position_argument
    def forward(self, pseudo_pos):
        bragg_angle, cr2latr_value, cr2vetr_value = self._calc_real_motor_pos(pseudo_pos)
        '''Run a forward (pseudo -> real) calculation'''
        return self.RealPosition(monobr   = bragg_angle,
                                 cr2vetr  = cr2vetr_value,
                                 cr2latr  = cr2latr_value,
                                 )

    @real_position_argument
    def inverse(self, real_pos):
        '''Run an inverse (real -> pseudo) calculation'''
        bragg_angle = real_pos.monobr
        wavelength = 2*mono_table[self.mono_type]['monod']*np.sin(np.deg2rad(bragg_angle))
        return self.PseudoPosition(energy =self.hc_over_e/wavelength,
                                   height = 2.0*(real_pos.cr2vetr-mono_table[self.mono_type]['c1height']-real_pos.cr2latr*mono_table[self.mono_type]['cr2vetrCorr'])*np.cos(np.deg2rad(bragg_angle)),
                                   dummy  = 0,  
                                   )



class DCMmySpot(Device):
    monoz          = Cpt(EpicsMotor, 'l0201002', labels={"dcm"})
    cr2latr        = Cpt(EpicsMotor, 'l0201003', labels={"dcm"})
    cr1roll        = Cpt(EpicsMotor, 'l0201004', labels={"dcm"})
    cr2roll        = Cpt(EpicsMotor, 'l0201005', labels={"dcm"})
    monobr         = Cpt(EpicsMotor, 'l0202000', labels={"dcm"})
    cr2vetr        = Cpt(EpicsMotor, 'l0202001', labels={"dcm"})
    monoy          = Cpt(EpicsMotor, 'l0202002', labels={"dcm"})
    cr2ropi        = Cpt(EpicsMotor, 'l0202003', labels={"dcm"})
    cr2yaw         = Cpt(EpicsMotor, 'l0202004', labels={"dcm"})
    monobr_encoder = Cpt(EpicsSignalRO, 'IK1380002', labels={"dcm"})

    p = Cpt(DCMEnergyHeight, "", labels={"dcm"})

    current_mono="Si111"



    


