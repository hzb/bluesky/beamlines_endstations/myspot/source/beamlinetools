
from ophyd import ( Component as Cpt, ADComponent, Device,
                    EpicsSignalRO,
                    SingleTrigger,
                    EigerDetector)
from ophyd.status import SubscriptionStatus
#import RE
import time as ttime
from types import SimpleNamespace
from ophyd.areadetector.base import ADComponent, EpicsSignalWithRBV
from ophyd import Component as Cpt, Signal
from ophyd.areadetector.filestore_mixins import FileStoreBase, new_short_uid
from datetime import datetime
from pathlib import PurePath


class EigerSimulatedFilePlugin(Device, FileStoreBase):
    sequence_id = ADComponent(EpicsSignalRO, 'SequenceId')
    file_path = ADComponent(EpicsSignalWithRBV, 'FilePath', string=True)
    file_write_name_pattern = ADComponent(EpicsSignalWithRBV, 'FWNamePattern',
                                          string=True)
    file_write_images_per_file = ADComponent(EpicsSignalWithRBV,
                                             'FWNImagesPerFile')
    current_run_start_uid = Cpt(Signal, value='', add_prefix=())
    enable = SimpleNamespace(get=lambda: True)

    def __init__(self, *args,redis_md=None, **kwargs):
        self.sequence_id_offset = 1
        # This is changed for when a datum is a slice
        # also used by ophyd
        self.redis_md = redis_md
        self.filestore_spec = "AD_EIGER2"
        self.frame_num = None
        super().__init__(*args, **kwargs)
        self._datum_kwargs_map = dict()  # store kwargs for each uid

    def stage(self):

        # Read the latest filepath root from redis
        self.write_path_template = self.redis_md['base_eiger']

        res_uid = new_short_uid()
        write_path = datetime.now().strftime(self.write_path_template)
        self.file_path.set(write_path + '/').wait()
        self.file_write_name_pattern.set(f'{write_path}/{res_uid}_$id').wait()
        super().stage()
        fn = (PurePath(self.file_path.get()) / res_uid)
        ipf = int(self.file_write_images_per_file.get())
        # logger.debug("Inserting resource with filename %s", fn)
        self._fn = fn
        res_kwargs = {'images_per_file' : ipf}
        self._generate_resource(res_kwargs)

    def generate_datum(self, key, timestamp, datum_kwargs):
        # The detector keeps its own counter which is uses label HDF5
        # sub-files.  We access that counter via the sequence_id
        # signal and stash it in the datum.
        seq_id = int(self.sequence_id_offset) + int(self.sequence_id.get())  # det writes to the NEXT one
        datum_kwargs.update({'seq_id': seq_id})
        if self.frame_num is not None:
            datum_kwargs.update({'frame_num': self.frame_num})
        return super().generate_datum(key, timestamp, datum_kwargs)

    def describe(self,):
        ret = super().describe()
        if hasattr(self.parent.cam, 'bit_depth'):
            cur_bits = self.parent.cam.bit_depth.get()
            dtype_str_map = {8: '|u1', 16: '<u2', 32:'<u4'}
            ret[self.parent._image_name]['dtype_str'] = dtype_str_map[cur_bits]
        return ret

class Eiger1(EigerDetector):

    file = Cpt(EigerSimulatedFilePlugin, suffix='cam1:',
               write_path_template='/user/mySpot/%Y_%m_%d_rock_it/',
               read_path_template='/eiger/user/mySpot/%Y_%m_%d_rock_it/', kind='normal')    

    def stage(self):
        super().stage()
        def check_value(*, old_value, value, **kwargs):
            return (old_value == 0 and value == 1)

        status = SubscriptionStatus(self.cam.armed, check_value, run=False, settle_time = 2, timeout=60)

        self.cam.acquire.set(1)
        print(f"Stage Called. Arming Eiger, please wait ...")

        return status
    
    def unstage(self):
        print(f"Unstage Called. Disarming Eiger, please wait ...")
        super().unstage()
        def check_value(*, old_value, value, **kwargs):
            return value == "Acquisition aborted"

        status_message = SubscriptionStatus(self.cam.status_message, check_value, run=False, settle_time = 2, timeout=60)

        status_message.wait(60)

        self.cam.acquire.set(0)
        

        return status_message

    def trigger(self):

        def check_value(*, old_value, value, **kwargs):
            return value == 0 or (old_value == value - 1)
        
        status_image_counter = SubscriptionStatus(self.cam.num_images_counter, check_value, run=False, settle_time = 0.5, timeout = 600) # Max integration time is 10 mins

        self.cam.special_trigger_button.set(1)

        return status_image_counter
    
class EigerManualTrigger(SingleTrigger, Eiger1):
    '''
        Like Eiger Single Trigger but the triggering is done through the
        special trigger button.
    '''
    def __init__(self, *args, **kwargs):
        self._set_st = None
        super().__init__(*args, **kwargs)
        # in this case, we don't need this + 1 sequence id cludge
        # this is because we write datum after image is acquired
        self.file.sequence_id_offset = 0
        self.file.filestore_spec = "AD_EIGER2"
        self.file.frame_num = 0

        # monkey patch
        # override with special trigger button, not acquire
        #self._acquisition_signal = self.special_trigger_button

    def initialize(self):
        self.file.kind = 'hinted'
        self.cam.trigger_mode.set(0) # this set it to internal series, EIGER_trigger_mode = ME[4]det
        self.cam.num_images.set(1) # EIGER_nimages = int(ME[5])
        self.cam.fw_num_images_per_file.set(1)
        self.cam.compression_algo.set(1) # set compression method to bslz4
        self.cam.beam_center_y.set(1500)
        self.cam.beam_center_x.set(1500)
        self.cam.num_triggers.set(1000)
        self.cam.manual_trigger.set(1)
        while(self.cam.manual_trigger.get() != 1):
            ttime.sleep(1)

    def stage(self):
        self.file.frame_num = 0
        super().stage()
        ttime.sleep(1)


    def unstage(self):
        self.file.frame_num = 0
        super().unstage()


    def trigger(self):
        ''' custom trigger for Eiger Manual'''
        def check_value(*, old_value, value, **kwargs):
            return value == 0 or (old_value == value - 1)
        
        status_image_counter = SubscriptionStatus(self.cam.num_images_counter, check_value, run=False, settle_time = 0.5, timeout = 500)

        self.cam.special_trigger_button.set(1)

        self.generate_datum(self._image_name, ttime.time())
        self.file.frame_num += 1

        return status_image_counter