import numpy as np
import os

from ophyd import Device, EpicsMotor
from ophyd import Component as Cpt


class Beamstop(Device):

    x = Cpt(EpicsMotor, "mot6", name="x")
    y = Cpt(EpicsMotor, "mot7", name="y")
    z = Cpt(EpicsMotor, "mot4", name="z")

    def __init__(self, *args, **kwargs):
        self.file_loc = '/opt/bluesky/beamlinetools/beamlinetools/config_positions/beamstop_position.txt'
        self._update_in_position()
        super().__init__(*args, **kwargs)

    def move_out(self):
        self.y.move(self.out_position)

    def move_in(self):
        
        self.y.move(self.in_position)

    def _update_in_position(self):
        if os.path.exists(self.file_loc):
            self.in_position = np.loadtxt(self.file_loc)
            
            self.out_position = self.in_position -5
            
        else:    
            print("No saved in position for the beamstop, save it using save_new_in_position")
            self.in_position = None
            self.out_position = None

    def ok(self):
        self.in_position = self.y.user_readback.get()
        np.savetxt(self.file_loc, np.array([self.in_position]))
        
        self.out_position = self.in_position -5
        