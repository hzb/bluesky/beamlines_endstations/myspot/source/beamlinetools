import numpy as np


from ophyd.mca import ROI

from ophyd import EpicsSignal, EpicsSignalRO
from ophyd.status import SubscriptionStatus, AndStatus 
from ophyd.device import DynamicDeviceComponent as DDC
from ophyd.device import Device, Component as Cpt, DynamicDeviceComponent
from ophyd import FormattedComponent as FCpt

from ophyd.signal import DerivedSignal 


class DivideByArgs(DerivedSignal):
    def __init__(self, derived_from, divisors, log=False,  write_access=None, name=None, parent=None, **kwargs):
        if divisors.__class__ is not list:
            raise ValueError(f"divisors should be a list, while it is a {type(divisors)}") 
        if len(divisors) == 0:
            raise ValueError(f"The divisors list should contain at least one element")
        self._divisors = divisors
        
        if log.__class__ is not bool:
            raise ValueError(f"log should be a bool, while it is a {type(log)}") 
        self._log = log
        super().__init__(derived_from, write_access=None, name=None, parent=None, **kwargs)

    def inverse(self, value):
        """Compute original signal value -> derived signal value"""
        divisor = 1
        for div in self._divisors:
            divisor *= div.get()
        value = value / divisor
        if self._log:
            value = np.log(value)
        return value

    def forward(self, value):
        """Compute derived signal value -> original signal value"""
        return value

    
# class DerivedSignals(Device):

    
#     def __init__(self, prefix, *, mca_ch_roi=None, mca_ch_lifetime=None, kth=None, read_attrs=None, configuration_attrs=None,
#                  name=None, parent=None, **kwargs):
#         self.lt = DivideByArgs(mca_ch_roi.count, [kth.readback], name="div_det2")
#         self.ltcur = DivideByArgs(mca_ch_roi.count, [mca_ch_lifetime, kth.readback], name="div_det2")
#         self.ltcurlog = DivideByArgs(mca_ch_roi.count, [mca_ch_lifetime, kth.readback], log=True, name="div_det2")
#         super().__init__(prefix, read_attrs=read_attrs,
#                          configuration_attrs=configuration_attrs,
#                          name=name, parent=parent, **kwargs)
        

# class DerivedROI(Device):

    
#     def __init__(self, prefix, *, mca_ch=None,kth=None, read_attrs=None, configuration_attrs=None,
#                  name=None, parent=None, **kwargs):
#         self.roi0 = DerivedSignals('', mca_ch_roi=mca_ch.roi0, mca_ch_lifetime=mca_ch.lifetime, kth=kth)
#         self.roi1 = DerivedSignals('', mca_ch_roi=mca_ch.roi1, mca_ch_lifetime=mca_ch.lifetime, kth=kth)
#         self.roi2 = DerivedSignals('', mca_ch_roi=mca_ch.roi2, mca_ch_lifetime=mca_ch.lifetime, kth=kth)
#         super().__init__(prefix, read_attrs=read_attrs,
#                          configuration_attrs=configuration_attrs,
#                          name=name, parent=parent, **kwargs)
        
# class DerivedMCA(Device):

    
#     def __init__(self, prefix, *, mca=None,kth=None, read_attrs=None, configuration_attrs=None,
#                  name=None, parent=None, **kwargs):
#         self.ch1 = DerivedROI('', mca_ch=mca.ch1, kth=kth)
#         self.ch2 = DerivedROI('', mca_ch=mca.ch1, kth=kth)
#         self.ch3 = DerivedROI('', mca_ch=mca.ch1, kth=kth)
#         super().__init__(prefix, read_attrs=read_attrs,
#                          configuration_attrs=configuration_attrs,
#                          name=name, parent=parent, **kwargs)
    
class Tunnel():
    def __init__(self, **kwargs):#derived_from=None, divisors=None, log=False, **kwargs) -> None:
        DivideByArgs(derived_from, divisors, log=log)

from beamlinetools.beamline_config.beamline import mca, kth00 as kth

class DerivedSignals(Device):
    lt       = Cpt(Tunnel,'', derived_from=mca.ch1.roi0.count, divisors=[kth.readback])
    # ltcur    = Cpt(Tunnel,'', derived_from=mca.ch1.roi0.count, divisors=[mca.ch1.lifetime, kth.readback])
    # ltcurlog = Cpt(Tunnel,'', derived_from=mca.ch1.roi0.count, divisors=[mca.ch1.lifetime, kth.readback], log=True)
        
    def __init__(self, prefix, *, read_attrs=None, configuration_attrs=None,
                 name=None, parent=None, **kwargs):

        super().__init__(prefix, read_attrs=read_attrs,
                         configuration_attrs=configuration_attrs,
                         name=name, parent=parent, **kwargs)
        

class DerivedROI(Device):
    def __init__(self, prefix, *, mca_ch=None,kth=None, read_attrs=None, configuration_attrs=None,
                 name=None, parent=None, **kwargs):
        self.roi0 = DerivedSignals('', mca_ch_roi=mca_ch.roi0, mca_ch_lifetime=mca_ch.lifetime, kth=kth)
        self.roi1 = DerivedSignals('', mca_ch_roi=mca_ch.roi1, mca_ch_lifetime=mca_ch.lifetime, kth=kth)
        self.roi2 = DerivedSignals('', mca_ch_roi=mca_ch.roi2, mca_ch_lifetime=mca_ch.lifetime, kth=kth)
        super().__init__(prefix, read_attrs=read_attrs,
                         configuration_attrs=configuration_attrs,
                         name=name, parent=parent, **kwargs)
        
class DerivedMCA(Device):

    
    def __init__(self, prefix, *, mca=None,kth=None, read_attrs=None, configuration_attrs=None,
                 name=None, parent=None, **kwargs):
        self.ch1 = DerivedROI('', mca_ch=mca.ch1, kth=kth)
        self.ch2 = DerivedROI('', mca_ch=mca.ch1, kth=kth)
        self.ch3 = DerivedROI('', mca_ch=mca.ch1, kth=kth)
        super().__init__(prefix, read_attrs=read_attrs,
                         configuration_attrs=configuration_attrs,
                         name=name, parent=parent, **kwargs)