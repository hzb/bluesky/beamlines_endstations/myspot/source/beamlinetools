import numpy as np
import os

from ophyd import Device, EpicsMotor
from ophyd import Component as Cpt


class DiodeMotor(EpicsMotor):
    """We need to save and be able to update the position declared as in the beam. 
    We achieve this by saving the in position to a file in beamlinetools/diode_position"""

    def __init__(self, *args, **kwargs):
        self.file_loc = '/opt/bluesky/beamlinetools/beamlinetools/config_positions/diode_position.txt'
        self._update_in_position()
        self.out_position = 260
        
        super().__init__(*args, **kwargs)

    def move_out(self):
        self.move(self.out_position)

    def move_in(self):
        
        self.move(self.in_position)

    def _update_in_position(self):
        if os.path.exists(self.file_loc):
            self.in_position = np.loadtxt(self.file_loc)
        else:    
            print("No saved in position for the diode, save it using save_new_in_position")
            self.in_position = None

    def ok(self):
        self.in_position = self.user_readback.get()
        np.savetxt(self.file_loc, np.array([self.in_position]))


