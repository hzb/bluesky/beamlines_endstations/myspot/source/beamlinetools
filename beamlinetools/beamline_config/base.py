from bluesky import RunEngine
from os.path import expanduser
from tiled.client import from_uri
from IPython import get_ipython

# Check if we are in ipython shell
is_ipython = get_ipython()

is_ipython.run_line_magic('logstart', '-o -r -t /opt/bluesky/data/log/ipython_logs.log append')

#import logging
#from logging.handlers import RotatingFileHandler
# Set up logging to capture IPython logs
#log_file = '/opt/bluesky/data/log/python_log.log'

# Set maximum size for log file (in bytes)
#max_log_size_gb = 1  # 1 gigabyte
#max_log_size = max_log_size_gb * 1024 * 1024 * 1024  # Convert gigabytes to bytes

# Create a RotatingFileHandler
#log_handler = RotatingFileHandler(log_file, mode='a', maxBytes=max_log_size, backupCount=5)

# Set up formatter
#formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

# Get logger for the root
#logger = logging.getLogger()

# Set logging level to capture only messages of INFO level or higher
#logger.setLevel(logging.INFO) 

# Add handler to the logger
#logger.addHandler(log_handler)

# Add handler to the root logger
#logging.root.addHandler(log_handler)


RE = RunEngine({})

from bessyii.callbacks.best_effort import BestEffortCallback
bec = BestEffortCallback()

# disabled bec plotting because it didn't work with the mca. We don't have time to debug now
bec.disable_plots()

# Send all metadata/data captured to the BestEffortCallback.
RE.subscribe(bec)

from os import environ
if environ.get('TILED_URL') is not None and environ.get('TILED_API_KEY') is not None:
    if "http" in environ.get('TILED_URL'):
        db = from_uri(environ.get('TILED_URL'), api_key=environ.get('TILED_API_KEY'))

from bluesky.callbacks.zmq import Publisher
# #define ip and port for the publisher
_host = environ.get('ZMQ_URL', "bsc2e2s2x.exp0202.bessy.de:5577" )
print(environ.get('ZMQ_URL'))
# #create publisher to broadcast documents via 0mq
publisher = Publisher(_host) 

# #subscribe the publisher to the document stream
RE.subscribe(publisher)


# Configure persistence between sessions of metadata
# change beamline_name to the name of the beamline
from bluesky.utils import PersistentDict
import os
cwd = os.getcwd()



from redis_json_dict import RedisJSONDict as _RedisJSONDict
import redis as _redis
class RedisPersistentDict(_RedisJSONDict):
    """
    A MutableMapping which syncs its contents to Redis.

    Create persistent dictionary with a new redis client
    >>> d = RedisPersistentDict(host='localhost', port=6379, prefix='my_prefix')

    RedisPersistentDict supports all keyword arguments in redis.Redis() for
    creating a new Redis client. Alternatively, an existing Redis client can
    be specified using 'redis_client' argument.

    Create persistent dictionary using an existing redis client
    >>> d = RedisPersistentDict(redis_client=redis_client, prefix='my_prefix')
    """

    def __init__(self, *args, prefix="bluesky_metadata_", redis_client=None, **kwargs) -> None:
        if redis_client is not None:
            if not isinstance(redis_client, _redis.client.Redis):
                raise TypeError("'redis_client' must be a valid Redis client object (redis.client.Redis).")
            if kwargs:
                warnings.warn("Keyrord arguments are ignored since 'redis_client' is specified.")  # noqa: B028
        if not isinstance(prefix, str):
            raise TypeError("'prefix' must be a valid non-empty string.")
        if not prefix:
            raise ValueError("'prefix' must be a valid non-empty string.")
        self._prefix = prefix
        self._redis_client = redis_client if redis_client else _redis.Redis(*args, **kwargs)
        super().__init__(redis_client=self._redis_client, prefix=self._prefix)

    @property
    def prefix(self):
        return self._prefix

    @property
    def redis_client(self):
        return self._redis_client

    def __repr__(self):
        return super().__repr__()

    def __iter__(self):
        yield from super().__iter__()

    def __len__(self):
        return super().__len__()

    def __getitem__(self, key):
        return super().__getitem__(key)

    def __setitem__(self, key, value):
        return super().__setitem__(key, value)

    def __delitem__(self, key):
        return super().__delitem__(key)

    def clear(self):
        return super().clear()

    def update(self, d):
        return super().update(d)

    def __copy__(self):
        return super().__copy__()

    def __deepcopy__(self, memo):
        return super().__deepcopy__(memo)


redis_dict =  RedisPersistentDict(host='localhost', port=6379, prefix='bluesky_metadata')
RE.md = PersistentDict(expanduser('/opt/bluesky/data/persistence/beamline/'))
RE.md = redis_dict