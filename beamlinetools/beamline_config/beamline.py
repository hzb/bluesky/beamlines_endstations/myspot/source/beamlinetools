"""Module:      beamline.py

Description:    Instantiate beamline devices. Instatiation is based on device configuration y(a)ml file and rml file.
                Global variables are created to have direct access to instantiated devices using just instance names, e.g. v3.
                Access to instantiated devices by means of a dictionary "devices_dictionary" is always possible, e.g. devices_dictionary["v3"]
"""

# Import functions necessary for beamline device instantiation
from bessyii_devices_instantiation.beamline_devices import (  
    instantiate,
    wait_for_device_connection,
)

from bessyii.utils.helper import add_to_global_scope
from .base import *

print('\n\nLOADING beamline.py')
# Set pyepics timeout
from ophyd.signal import EpicsSignalBase
EpicsSignalBase.set_defaults(connection_timeout= 5, timeout=5)

# Instantiate beamline devices
devices_dictionary: dict[object] = instantiate()

# Wait for instantiated devices to be connected
wait_for_device_connection(devices_dictionary)

# Create global variables from the "devices"
add_to_global_scope(devices_dictionary, globals())


from secop_ophyd.SECoPDevices import SECoPNodeDevice


#gas_system = SECoPNodeDevice.create('172.31.21.211','10800',RE.loop) # This is a simulation
#reactor_cell = SECoPNodeDevice.create('172.31.21.211','10801',RE.loop) # This is a simulation
print("connecting to SECoP devices...")
gas_analysis = SECoPNodeDevice.create('172.31.21.237', '2201', RE.loop) # This is the real 


gas_system = SECoPNodeDevice.create('172.31.21.240','2202',RE.loop) # This is the real 
reactor_cell = SECoPNodeDevice.create('172.31.21.240','2201',RE.loop) # This is the real 
print("connected.")

temp_controller_temperature_sam = reactor_cell.temperature_sam
temp_controller_temperature_reg = reactor_cell.temperature_reg

gas_analysis.mass_spec.add_readables(
    [gas_analysis.mass_spec.vacuum_pressure,gas_analysis.mass_spec.mass])

from ophyd.signal import EpicsSignalRO

dcm_encoder = EpicsSignalRO("DCM1OS2L:IK1380002", name="dcm_encoder", kind = "normal")

### eiger config
try:
    # Attempt to pass the redis md persistent dict to the eiger device
    eiger.file.redis_md = redis_dict
except Exception as e:
    print(f"Eiger not instantiated or redis_dict not imported from base: {e}")



# special settings
for device in devices_dictionary.values():  
    if device.name == 'mca':
        device.ch1.roi0.display()
        device.ch1.roi1.display()
        device.ch1.roi2.display()
        device.ch1.roi3.display()
        device.ch1.roi4.display()
        device.ch1.roi5.display()
        device.ch1.roi6.display()
        device.ch1.roi7.display()
        device.ch1.realtime.kind='hinted'
