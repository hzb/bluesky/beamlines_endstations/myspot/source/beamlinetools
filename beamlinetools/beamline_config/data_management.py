
from beamlinetools.beamline_config.base import RE, db
#from beamlinetools.beamline_config.beamline import dcm
from beamlinetools.callbacks.mca_callback import McaCallback
from beamlinetools.data_management.data_structure import MySpotDataStructure    


# MySpot Data structure folder
mds = MySpotDataStructure(RE)


# qserver to RE function for  for data structure in GUI tab, may move on an other file
def qs_write_to_re_md(key, value):
    """
    This  function allows to write to the RE metadata dictionary, it updates existing keys or adds new keys triggered
    by the qserver cli. This way then RE metadata can be manipulated by the GUI.

    parameters:
    key: str
        The key to be updated or added
    value: str
        The value for the key to be updated or added

    """

    print(f"key: {key}, value: {value}  seved to RE.md")

    RE.md[key] = value

def qs_read_from_re_md(key):
    """
    This  function allows to read from the RE metadata dictionary, it reads the value of the key triggered
    by the qserver cli. This way then RE metadata can be read by the GUI.

    parameters:
    key: str
        The key to be read

    """    
    md_entry = RE.md[key]
    print(md_entry)
    # return md_entry,{'res': md_entry}
    return md_entry   

from beamlinetools.data_management.specfile_management import spec_factory

def qs_set_spec_factory_prefixes(base_name, newdata_dir):
    # change specifle name and path
    spec_factory.file_prefix = base_name
    spec_factory.directory = newdata_dir

def qs_set_md_paths():
    try:
        from beamlinetools.beamline_config.beamline import eiger
        eiger.file.write_path_template = RE.md['base_eiger']
        print(f"Set the eiger write path template to {eiger.file.write_path_template}")
    except:
        raise ValueError('Eiger is not installed')

def qs_set_csv_export_folder(path):
    # set the path for csvcallback 

    # We have commented this, because we are only using specfile writing for now. There was a bug with csv writing maybe with mca/eiger? Will
    #mds.csv.file_path=path
    pass

    


#MCA Callback
mca_data_export_callback = McaCallback()
RE.subscribe(mca_data_export_callback)


# eiger preprocessor
#from beamlinetools.preprocessors.eiger_preprocessor import SupplemetalDataEiger
#from .base import RE

#sde = SupplemetalDataEiger(dcm=dcm, RE=RE)
#RE.preprocessors.append(sde)

from beamlinetools.preprocessors.inject_metadata_preprocessor import FetchExperimentMetadata
RE.preprocessors.append(FetchExperimentMetadata(filepath="/opt/bluesky/beamlinetools/beamlinetools/beamline_config/experiment_config.yaml"))
