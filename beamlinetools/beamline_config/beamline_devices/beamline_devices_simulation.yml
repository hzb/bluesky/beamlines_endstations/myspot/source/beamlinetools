# File name:      beamline_devices_simulation.yml
#
# Description:    List of devices applied at given beamline and their configuration parameters.
#
# Usage:          Add/remove/configure devices according to beamline setup.
#
# Comment:        This file is necessary for creation of happi database and based on that instantation of devices at the beamline.
#
# Explanation:    Each device configured in this file containes a number of properties, organized in key/value pairs (e.g. "active: True").
#                 Eight properties' keys (listed below) are mandatory for all devices since necessary for instanciation process of devices.
#                 Two of the five properties must be unique: "prefix" and "instance_name".
#                 Additional properties of the devices can be added. They are facultative.
#
#                 Each property can be retrieved from an instantiated device (obj) by applying "md". E.g. "obj.md.active"
#
# Nine mandatory properties:
#
#            1.     "type",                 mandatory, not empty, not unique. Device type. A good practice could be to use a 'type' of object(device) from rml file if given device is present there or alternatively name of the class the device belongs to,
#                                           e.g. "PositionerBessyValve" or any other arbitrary value.
#            2.     "active"                mandatory, not empty, not unique. Device is active/enabled: {True, False}. Active/enabled device is present in database and can be instantiated. Inactive/disabled device is present in database but will not be instantiated.
#            3.     "class",                mandatory, not empty, not unique. Class name necessary to instantiate object, e.g. "bessyii_devices.valve.PositionerBessyValve".
#            4.     "prefix",               mandatory, not empty, unique.     Name of the PV, e.g. "V03V11B001L:".
#            5.     "instance_name",        mandatory, not empty, unique,     Instance name, e.g. "v3".
#            6.     "class_kwargs",         mandatory, not empty if class instantiation requires kwargs, empty if class does not require kwargs, in empty case please write only "class_kwargs:" or "class_kwargs: []", not unique.
#                                                 Example of class_kwargs if class instantiation requires kwargs:
#
#                                                 class_kwargs:
#                                                   - read_attrs:                 I represent a list: read_attrs["readback", "reset"]
#                                                       - "readback"
#                                                       - "reset"
#                                                   - labels:                     I represent a list: labels["detectors"], even though labels should be a set, the __init__ of class OphydObject converts it explicitelly to a set, so we can use a list !
#                                                       - "detectors"
#                                                   - s_labels: s_label_value     I represent "simple" key:value
#
#
#                                                 Example of class_kwargs if class instantiation does not require kwargs:
#
#                                                   class_kwargs: or class_kwargs: []
#
#
#              7.   "connection_timeout",   mandatory, not empty, not unique. Timeout value used in ophyd function wait_for_connection(). Allowed input types: float.
#              8.   "apply_rml_on",         mandatory, not empty, not unique. Extraction of metadata from rml-file and its incorporation into happi container {True, False}
#              9.   "nx_class"              mandatory, not empty, not unique. Device class name for NeXus writer. NX Writer needs to know to which NXclass belongs a given device instance.
#
# Arbitrary number of facultative properties:
#
#                 "key": "value",           facultative, "key" is arbitrary but different from mandatory properties' keys names, "value" is arbitrary. Example
#
#
# Facultative properties evaluated in baseline.py:
#              1.   "baseline",             facultative, not empty, not unique. Add device to the list "baseline": {True, False}. The baseline readings to be taken automatically before and after every run.
#              2.   "silent",               facultative, not empty, not unique. Add device to the list "silent_devices": {True, False}. The devices to be read during run, but not plotted unless explicitly set in the plan.
#
#
#
# ---------------------------------- Device configuration -----------------------------------

#  --- Summary of configured devices ---
# 1.  m1    (ophyd.sim.SynAxis)
# 2.  m2    (ophyd.sim.SynAxis)
#  --- End of Summary of configured devices ---

# Simulated motors
- type: SynAxis # I am mandatory property
  active: True # I am mandatory property
  class: ophyd.sim.SynAxis # I am mandatory property
  prefix: "" # I am mandatory property
  instance_name: m1 # I am mandatory property
  class_kwargs: # I am mandatory property
    - labels:
        - "motors"
  connection_timeout: 2 # I am mandatory property
  apply_rml_on: True # I am mandatory property
  nx_class: someNxClass # I am mandatory property
  desc: "I am SynAxis, called m1" # I am facultative property
  myMetaData: True # I am facultative property
  myMetaData2: -123456.4567 # I am facultative property
  myMetaData3: Hello from m1 # I am facultative property
  baseline: True # I am facultative property

- type: SynAxis # I am mandatory property
  active: True # I am mandatory property
  class: ophyd.sim.SynAxis # I am mandatory property
  prefix: "" # I am mandatory property
  instance_name: m2 # I am mandatory property
  class_kwargs: # I am mandatory property
    - labels:
        - "motors"
  connection_timeout: 2 # I am mandatory property
  apply_rml_on: True # I am mandatory property
  nx_class: someNxClass # I am mandatory property
  desc: "I am SynAxis, called m2" # I am facultative property
  myMetaData: False # I am facultative property
  myMetaData2: -321.67 # I am facultative property
  myMetaData3: Hello from m2 # I am facultative property
  baseline: True # I am facultative property
  silent: True # I am facultative property
# ---------------------------------- End of Device configuration -----------------------------------
