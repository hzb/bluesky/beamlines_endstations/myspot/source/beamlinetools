# This is a file for plans that should not have magics created
from beamlinetools.plans.alignment_plans import align_second_crystal_pitch, align_second_crystal_roll
#from beamlinetools.plans.check_plans import check_beamstop, open_rollo, close_rollo

from beamlinetools.plans.qserver_plans import set_pressure, ramp, dwell, count_detectors, xrd, sleep 
from beamlinetools.plans.xas_plans import exafs, xas_single_region
from beamlinetools.plans.xas_plans import select_absorber